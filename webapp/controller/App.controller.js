sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageToast"
], function(Controller, JSONModel, Filter, FilterOperator, MessageToast) {
	"use strict";

	return Controller.extend("sap.ui.demo.todo.controller.App", {

		onPress: function (evt) {
			console.log(MessageToast);
			MessageToast.show(evt.getSource().getId() + " Pressed");
		}

	});

});
